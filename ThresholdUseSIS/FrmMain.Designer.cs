﻿namespace HistGramEqualize
{
    partial class FrmMain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.CmdLoad = new System.Windows.Forms.Button();
            this.CmdConvertToGrayMode = new System.Windows.Forms.Button();
            this.CMDSIS = new System.Windows.Forms.Button();
            this.Pic = new System.Windows.Forms.PictureBox();
            this.LblInfo = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.Pic)).BeginInit();
            this.SuspendLayout();
            // 
            // CmdLoad
            // 
            this.CmdLoad.Location = new System.Drawing.Point(12, 12);
            this.CmdLoad.Name = "CmdLoad";
            this.CmdLoad.Size = new System.Drawing.Size(75, 23);
            this.CmdLoad.TabIndex = 4;
            this.CmdLoad.Text = "加载图像";
            this.CmdLoad.UseVisualStyleBackColor = true;
            this.CmdLoad.Click += new System.EventHandler(this.CmdLoad_Click);
            // 
            // CmdConvertToGrayMode
            // 
            this.CmdConvertToGrayMode.Location = new System.Drawing.Point(107, 12);
            this.CmdConvertToGrayMode.Name = "CmdConvertToGrayMode";
            this.CmdConvertToGrayMode.Size = new System.Drawing.Size(75, 23);
            this.CmdConvertToGrayMode.TabIndex = 6;
            this.CmdConvertToGrayMode.Text = "转化为灰度";
            this.CmdConvertToGrayMode.UseVisualStyleBackColor = true;
            this.CmdConvertToGrayMode.Click += new System.EventHandler(this.CmdConvertToGrayMode_Click);
            // 
            // CMDSIS
            // 
            this.CMDSIS.Location = new System.Drawing.Point(207, 12);
            this.CMDSIS.Name = "CMDSIS";
            this.CMDSIS.Size = new System.Drawing.Size(75, 23);
            this.CMDSIS.TabIndex = 7;
            this.CMDSIS.Text = "SIS阈值";
            this.CMDSIS.UseVisualStyleBackColor = true;
            this.CMDSIS.Click += new System.EventHandler(this.CMDSIS_Click);
            // 
            // Pic
            // 
            this.Pic.Image = global::ThresholdUseSIS.Properties.Resources.lena2;
            this.Pic.Location = new System.Drawing.Point(31, 41);
            this.Pic.Name = "Pic";
            this.Pic.Size = new System.Drawing.Size(934, 416);
            this.Pic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Pic.TabIndex = 5;
            this.Pic.TabStop = false;
            // 
            // LblInfo
            // 
            this.LblInfo.AutoSize = true;
            this.LblInfo.Location = new System.Drawing.Point(310, 17);
            this.LblInfo.Name = "LblInfo";
            this.LblInfo.Size = new System.Drawing.Size(0, 12);
            this.LblInfo.TabIndex = 8;
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(998, 501);
            this.Controls.Add(this.LblInfo);
            this.Controls.Add(this.CMDSIS);
            this.Controls.Add(this.CmdConvertToGrayMode);
            this.Controls.Add(this.Pic);
            this.Controls.Add(this.CmdLoad);
            this.Name = "FrmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SIS阈值法";
            ((System.ComponentModel.ISupportInitialize)(this.Pic)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button CmdLoad;
        private System.Windows.Forms.PictureBox Pic;
        private System.Windows.Forms.Button CmdConvertToGrayMode;
        private System.Windows.Forms.Button CMDSIS;
        private System.Windows.Forms.Label LblInfo;
    }
}

